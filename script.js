let switcher = document.querySelectorAll('.switch');

switcher.forEach((el)=>{
el.addEventListener('click', function(){
   changeTheme(this.dataset.theme);
   localStorage.setItem('theme', this.dataset.theme)
})
});
function changeTheme(themeName){
   
let themeUrl = `css/theme-${themeName}.css`;
document.querySelector('[title="theme"]').setAttribute('href', themeUrl);
}

let activeTheme = localStorage.getItem('theme');
if(activeTheme === null){
    changeTheme('light');
} else{
   changeTheme(activeTheme);
}